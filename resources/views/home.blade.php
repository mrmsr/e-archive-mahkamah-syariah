@extends('layout.app')

@section('main_content')
    <div class="jumbotron text-center">
        <img src="http://comradedeveloper.com/assets/images/logo/logo-putih.png" alt="E-ARSIP MAHKAMAH SYARI'AH">
        <h1>E-ARSIP MAHKAMAH SYAR'IYAH</h1>
        <p>
            PENATAAN ARSIP PERKARA/PUTUSAN MELALUI E-ARSIP PADA MAHKAMAH SYAR'IYAH BANDA ACEH KELAS-IA
        </p>
        <a href="{{ route('pencarian_arsip') }}" class="btn btn-lg btn-primary">Memulai</a>
        @if(!Auth::check())
            <a href="{{ route('login') }}" class="btn btn-lg btn-success">Login</a>
        @endif
    </div>
@endsection