{{-- Menu --}}

<div>
    <ul class="nav navbar-nav">
        <li class="{{ Request::is('pencarian_arsip') ? 'active' : '' }}">
            <a href="{{ route('pencarian_arsip') }}">
                <i class="fa fa-search"></i> Pencarian Arsip
            </a>
        </li>

        @if (Auth::check())
            <li class="dropdown {{ Request::is('operasi/*') ? 'active' : '' }}">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-edit"></i> Operasi
                    <b class="caret"></b>
                </a>
                <ul class="dropdown-menu">
                    <li><a href="{{ route('perkara') }}">Input Perkara</a></li>
                    <li class="divider"></li>
                    <li><a href="{{ route('amar_putusan') }}">Input Amar Putusan</a></li>
                    <li><a href="{{ route('salinan_putusan') }}">Pengambilan Salinan Putusan</a></li>
                    <li><a href="{{ route('akta_cerai') }}">Pengambilan Akta Cerai</a></li>
                </ul>
            </li>
        @endif
    </ul>
</div>
