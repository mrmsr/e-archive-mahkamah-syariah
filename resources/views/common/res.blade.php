
<!-- jQuery -->
<script src="{{asset('/res/vendor/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap -->
<script src="{{asset('/res/vendor/bootstrap/js/bootstrap.min.js')}}"></script>

<!-- jquery.inputmask -->
<script src="{{asset('/res/vendor/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js')}}"></script>
<!-- Input mask -->
<script>
    $(document).ready(function(){
        Inputmask().mask(document.querySelectorAll("input"));
    });
</script>
