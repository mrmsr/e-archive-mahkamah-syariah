 <nav class="navbar navbar-default" role="navigation">
     <div class="container-fluid">
         <div class="navbar-header">
             <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-menu-collapse" aria-expanded="false">
                 <span class="sr-only">Toggle navigation</span>
                 <span class="icon-bar"></span>
                 <span class="icon-bar"></span>
                 <span class="icon-bar"></span>
             </button>
             <a href="{{ url('/') }}" class="navbar-brand">E-Arsip Mahkamah Syar'iyah</a>
         </div>
         <div class="collapse navbar-collapse" id="navbar-menu-collapse">

             @include('common.menu')

             <ul class="nav navbar-nav navbar-right">
                 @if (Auth::check())
                     <li class="dropdown">
                         <a href="#" class="user-profile dropdown-toggle" data-toggle="dropdown"
                            aria-expanded="false">
                             {{--<img src="{{asset('/res/images/img.jpg')}}" alt="">--}}
                             Welcome, {{ Auth::user()->name }}
                             <span class="caret"></span>
                         </a>
                         <ul class="dropdown-menu dropdown-usermenu">
                             <li>
                                 <a href="{{ route('user_profile') }}">
                                     Profil
                                 </a>
                             </li>
                             <li>
                                 <a href="{{ url('ubah_password') }}">
                                     Ubah Password
                                 </a>
                             </li>
                             <li class="divider"></li>
                             <li><a href="{{ url('logout') }}" onclick="event.preventDefault();
                             document.getElementById('logout-form').submit();">
                                     <i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                         </ul>
                     </li>

                     <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                         {{ csrf_field() }}
                     </form>
                 @else
                     <li class="dropdown">
                         <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"
                            aria-expanded="false">
                             Welcome, Guest
                             <span class="caret"></span>
                         </a>
                         <ul class="dropdown-menu dropdown-usermenu">
                             <li><a href="{{ route('login')}}"><i class="fa fa-sign-in pull-right"></i> Login</a></li>
                             <li><a href="{{ url('/')}}"><i class="fa fa-sign-out pull-right"></i> Exit</a></li>
                         </ul>
                     </li>
                 @endif
             </ul>
         </div>

     </div>
</nav>