@extends('layout.app')

@section('content')

@include('page.operasi.common.success')
@include('page.operasi.common.delete_success')

<div>
    <div class="panel panel-primary">
        <div class="panel-heading">
            <div class="panel-title">
                Profil User Admin
            </div>
        </div>
        <div class="panel-body">
            @include('common.errors')

            <form class="form-horizontal" action="{{route('post_user_profile')}}" novalidate="" method="post">
                {{ csrf_field() }}
                <input type="hidden" name="id" value="{{Auth::user()->id}}">
                <div class="row item form-group @if($errors->has('nama')){{'has-error'}}@endif">
                    <label for="nama" class="control-label col-md-3">Nama</label>
                    <div class="col-md-6">
                        <input type="text"
                               value="{{ Auth::user()->name }}"
                               placeholder="Silahkan masukkan password sekarang"
                               name="nama" class="form-control">
                        @if($errors->has('nama'))
                            <p class="text-danger">
                                {{ $errors->first('old_password') }}
                            </p>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-6 col-md-offset-3">
                        <button type="submit" class="btn btn-success">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection