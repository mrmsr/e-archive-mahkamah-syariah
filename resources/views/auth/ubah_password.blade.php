@extends('layout.app')

@section('content')
    @include('page.operasi.common.success')
    @include('page.operasi.common.delete_success')

    <div class="">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="panel-title">
                    Ubah Password
                </div>
            </div>
            <div class="panel-body">
                @include('common.errors')

                <form class="form-horizontal" action="{{route('ubah_password')}}" novalidate="" method="post">
                    {{ csrf_field() }}
                    <input type="hidden" name="id" value="{{Auth::user()->id}}">
                    <div class="row item form-group @if($errors->has('password_lama')){{'has-error'}}@endif">
                        <label for="" class="control-label col-md-3">Password Lama</label>
                        <div class="col-md-6">
                            <input type="password"
                                   value=""
                                   placeholder="Silahkan masukkan password sekarang"
                                   name="old_password" class="form-control">
                            @if($errors->has('old_password'))
                                <p class="text-danger">
                                    {{ $errors->first('old_password') }}
                                </p>
                            @endif
                        </div>
                    </div>
                    <div class="row item form-group @if($errors->has('password_baru')){{'has-error'}}@endif">
                        <label for="" class="control-label col-md-3">Password Baru</label>
                        <div class="col-md-6">
                            <input type="password"
                                   value=""
                                   placeholder="Silahkan masukkan password baru anda"
                                   name="new_password" class="form-control">
                            @if($errors->has('new_password'))
                                <p class="text-danger">
                                    {{ $errors->first('new_password') }}
                                </p>
                            @endif
                        </div>
                    </div>
                    <div class="row item form-group @if($errors->has('password_baru')){{'has-error'}}@endif">
                        <label for="" class="control-label col-md-3">Konfirmasi Password Baru</label>
                        <div class="col-md-6">
                            <input type="password"
                                   value=""
                                   placeholder="Silahkan masukkan ulang password baru anda"
                                   name="confirm" class="form-control">
                            @if($errors->has('confirm'))
                                <p class="text-danger">
                                    {{ $errors->first('confirm') }}
                                </p>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                            <button type="submit" class="btn btn-success">Ubah</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
