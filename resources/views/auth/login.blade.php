@extends('layout.app')

@section('content')

    <div class="panel panel-primary">
        <div class="panel-heading">
            <div class="panel-title">
                Login
            </div>
        </div>
        <div class="panel-body">

            <form action="{{route('login')}}" class="form" method="post">
                <div class="row">
                    {{ csrf_field() }}
                    @if ($errors->has('message'))
                        <div class="col-md-12 alert alert-danger">
                            {{$errors->first('message')}}
                        </div>
                    @endif
                    <div class="form-group col-md-12">
                        <input type="text" name="username" class="form-control" placeholder="Username" required=""/>
                    </div>
                    <div class="form-group col-md-12">
                        <input type="password" name="password" class="form-control" placeholder="Password" required=""/>
                    </div>
                    <div class="form-group col-md-12 text-right">
                        <button type="submit" class="btn btn-default submit" href="index.html">Log in</button>
                    </div>
                </div>
            </form>

        </div>
    </div>
@endsection