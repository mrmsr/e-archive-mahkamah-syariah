<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <script src="{{ asset('/res/vendor/pace/pace.min.js') }}"></script>
    <link rel="stylesheet" href="{{ asset('/res/vendor/pace/pace-theme.css') }}">
    <style>
        .top-buffer {
            margin-top: 7px
        }

        .floating_alert {
            position: fixed;
            top: 50px;
            right: 20px;
            z-index: 5000;
        }
        .ln_solid {
            height: 1px;
            width: 100%;
            display: block;
            margin: 9px 0;
            overflow: hidden;
            background-color: #e5e5e5;
        }

        p, div, span, h1, h2, h3, h4, h5, h6 {
            font-family: Source Serif Pro, PT Sans, Trebuchet MS, Helvetica, Arial;
        }

    </style>

    <title>Arsip Perkara Mahkamah Syariah</title>

    <!-- Bootstrap -->
    <link href="{{ asset('/res/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{ asset('/res/vendor/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">

    {{--  Angular --}}
    <script src="{{asset('/res/vendor/angularjs/angular.min.js')}}"></script>
    <script src="{{asset('/res/vendor/angularjs/plugins/ui-bootstrap-tpls.min.js')}}" charset="utf-8"></script>
    <script src="{{asset('/res/vendor/angularjs/mask.min.js')}}"></script>
    <script type="text/javascript">
        var app = angular.module('eArchiveApp', ['ui.mask', 'ui.bootstrap']);
        app.controller('mainCtrl', function($scope) {
        });
    </script>
</head>

<body ng-app="eArchiveApp">

@include('common.header')

@yield('main_content')

<div class="container">
    @yield('content')
</div>
@include('common.footer')

@include('common.res')
@yield('additional_script')

</body>
</html>
