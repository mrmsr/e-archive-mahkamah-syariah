<div class="row">
    <b class="col-md-3 text-right">Nomor Perkara</b>
    <div class="col-md-9">
        {{ $perkara->nomor_perkara or "" }}
    </div>
</div>
<div class="row">
    <b class="col-md-3 text-right">Jenis Perkara</b>
    <div class="col-md-9">
        {{ $perkara->jenis_perkara or "" }}
    </div>
</div>
<div class="row">
    <b class="col-md-3 text-right">Nama Pemohon/Penggugat</b>
    <div class="col-md-9">
        {{ $perkara->nama_pemohon or "" }}
    </div>
</div>
<div class="row">
    <b class="col-md-3 text-right">Nama Termohon/Tergugat</b>
    <div class="col-md-9">
        {{ $perkara->nama_termohon or "" }}
    </div>
</div>
