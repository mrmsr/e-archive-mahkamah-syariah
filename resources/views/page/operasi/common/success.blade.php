@if (Session::has('success'))
    <div class="alert alert-success" data-dismiss="alert">
        {!! Session::get('success') !!}
    </div>
@endif
