<div ng-controller="cariCtrl">
    <div class="pull-right">
        <div class="form-group">
            <form action="{{$route}}" autocomplete="off">
                <div class="input-group">
                    <input class="form-control typeahead"
                        ng-model="nomor_perkara"
                        uib-typeahead="data.nomor_perkara for data in cariNomorPerkara($viewValue)"
                        typeahead-wait-ms="300"
                        typeahead-no-results="noResult"
                        typeahead-loading="loading"
                           placeholder="Masukkan nomor perkara"
                           title="Silahkan masukkan nomor perkara untuk mengubah data perkara dengan nomor tersebut."
                        name="nomor_perkara"
                           type="text">
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="submit">Cari</button>
                    </span>
                </div>
                <div class="text-danger" ng-if="noResult">
                    Tidak ada hasil.
                </div>
                <div class="text-info" ng-if="loading">
                    Loading...
                </div>
            </form>
        </div>
    </div>
    <div class="clearfix"></div>
</div>

<script type="text/javascript">
    angular.module('eArchiveApp').controller('cariCtrl', function($scope,$http) {
        $scope.cariNomorPerkara = function(viewValue) {
            return $http({
                method: 'GET',
                url: '{{ route('cari_nomor_perkara') }}',
                params: {
                    key: viewValue,
                },
            }).then(function(resp) {
                return resp.data
            })
        }
    })
</script>
