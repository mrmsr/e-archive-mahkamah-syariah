@if (Session::has('delete_success'))
    <div class="alert alert-warning" data-dismiss="alert">
        {!! Session::get('delete_success') !!}
    </div>
@endif
