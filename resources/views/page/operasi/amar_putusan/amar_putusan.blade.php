@extends('layout.app')

@section('content')


    @include('page.operasi.common.success')
    @include('page.operasi.common.delete_success')


    <div class="floating_alert">
        @include('common.errors')
    </div>

    <div ng-controller="mainCtrl">
        @include('page.operasi.common.title', ['title' => 'Amar Putusan'])
        @include('page.operasi.common.cari_nomor_perkara', ['route' => route('amar_putusan')])

        @if ($perkara != '')
            {{-- Data tersimpan --}}
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title">
                        Data Tersimpan
                    </div>
                </div>
                <div class="panel-body">

                    <form class="form-horizontal" novalidate="">
                        <h4 class="text-center text-info">Perkara</h4>
                        <div class="ln_solid"></div>

                        @include('page.operasi.common.common_data')

                        <h4 class="text-center text-info">Amar Putusan</h4>
                        <div class="ln_solid"></div>

                        <div class="row">
                            <b class="text-right col-md-3" >Amar Putusan</b>
                            <div class="col-md-6">
                                @if ($ap = $perkara->amarPutusan()->first())
                                    {!!  nl2br($ap->amar_putusan)  !!}
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <b class="text-right col-md-3" >Tanggal Putus</b>
                            <div class="col-md-6">
                                {{ $perkara->amarPutusan->tanggal_putus or "" }}
                            </div>
                        </div>
                        <div class="row">
                            <b class="text-right col-md-3" >Tanggal BHT</b>
                            <div class="col-md-6">
                                {{ $perkara->amarPutusan->tanggal_bht or "" }}
                            </div>
                        </div>
                        <div class="row">
                            <b class="text-right col-md-3" >Status Perkara</b>
                            <div class="col-md-6">
                                {{ $perkara->status_perkara or "" }}
                            </div>
                        </div>
                        <div class="row">
                            <b class="text-right col-md-3" >Letak Berkas</b>
                            <div class="col-md-6">
                                {{ $perkara->letakBerkas->letak_berkas or "" }}
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            {{-- Ubah data --}}
            <div class="panel panel-success">
                <div class="panel-heading">
                    <div class="panel-title">
                        Ubah Amar Putusan
                        <div class="pull-right">
                            <a href="{{route('amar_putusan_delete', ['id' => "$perkara->id"])}}">
                                <i
                                        class="fa fa-trash"></i>
                            </a>
                        </div>

                    </div>

                </div>
                <div class="panel-body">

                    <form class="form-horizontal" action="{{route('amar_putusan_edit')}}" novalidate="" method="post">
                        @include('common.errors')
                        {{ csrf_field() }}
                        <input type="hidden" value="{{ $perkara->id }}" name="id">
                        <div class="row item form-group @if($errors->has('amar_putusan')){{'has-error'}}@endif">
                            <label for="" class="control-label col-md-3">Amar Putusan</label>
                            <div class="col-md-6">
                                <textarea class="form-control" name="amar_putusan">@if(old('amar_putusan') != ""){{old('amar_putusan')}}@else{{$perkara->amar_putusan or ""}}@endif</textarea>
                                @if($errors->has('amar_putusan'))
                                    <p class="text-danger">
                                        {{ $errors->first('amar_putusan') }}
                                    </p>
                                @endif
                            </div>
                        </div>
                        <div class="row item form-group @if($errors->has('tanggal_putus')){{'has-error'}}@endif">
                            <label for="" class="control-label col-md-3">Tanggal Putus</label>
                            <div class="col-md-6">
                                <input type="text"
                                       value="@if(old('tanggal_putus') != ""){{old('tanggal_putus')}}@else{{$perkara->tanggal_putus or ""}}@endif"
                                       name="tanggal_putus" class="form-control" data-inputmask="'mask': 'd/m/y'" placeholder="dd/mm/yyyy">
                                @if($errors->has('tanggal_putus'))
                                    <p class="text-danger">
                                        {{ $errors->first('tanggal_putus') }}
                                    </p>
                                @endif
                            </div>
                        </div>
                        <div class="row item form-group @if($errors->has('tanggal_bht')){{'has-error'}}@endif">
                            <label for="" class="control-label col-md-3">Tanggal BHT</label>
                            <div class="col-md-6">
                                <input type="text"
                                       value="@if(old('tanggal_bht') != ""){{old('tanggal_bht')}}@else{{$perkara->tanggal_bht or ""}}@endif"
                                       name="tanggal_bht" class="form-control" data-inputmask="'mask': 'd/m/y'"  placeholder="dd/mm/yyyy">
                                @if($errors->has('tanggal_bht'))
                                    <p class="text-danger">
                                        {{ $errors->first('tanggal_bht') }}
                                    </p>
                                @endif
                            </div>
                        </div>
                        <div class="row item form-group @if($errors->has('status_perkara')){{'has-error'}}@endif">
                            <label for="" class="control-label col-md-3">Status Perkara</label>
                            <div class="col-md-6">
                                <div ng-hide="true" ng-init="status_perkara = '@if(old('status_perkara') != ""){{old('status_perkara')}}@else{{$perkara->status_perkara or "statis"}}@endif'"></div>
                                <div class="radio-inline">
                                    <label>
                                        <input ng-model="status_perkara" name="status_perkara" type="radio" value="statis" checked> Statis
                                    </label>
                                </div>
                                <div class="radio-inline">
                                    <label >
                                        <input ng-model="status_perkara" name="status_perkara" type="radio" value="dinamis"> Dinamis
                                    </label>
                                </div>
                                @if($errors->has('status_perkara'))
                                    <p class="text-danger">
                                        {{ $errors->first('status_perkara') }}
                                    </p>
                                @endif
                            </div>
                        </div>
                        <div class="row item form-group @if($errors->has('letak_berkas')){{'has-error'}}@endif" ng-if="status_perkara == 'statis'">
                            <label for="" class="control-label col-md-3">Letak Berkas</label>
                            <div class="col-md-6">
                                <input type="text"
                                       value="@if(old('letak_berkas') != ""){{old('letak_berkas')}}@else{{$perkara->letak_berkas or ""}}@endif"
                                       class="form-control" name="letak_berkas" >
                                @if($errors->has('letak_berkas'))
                                    <p class="text-danger">
                                        {{ $errors->first('letak_berkas') }}
                                    </p>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-3">
                                <button type="submit" class="btn btn-success">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        @endif

    </div>
@endsection
