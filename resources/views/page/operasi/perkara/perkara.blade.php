@extends('layout.app')

@section('additional_script')
    @include('page.operasi.perkara.angular_perkara')
@endsection

@section('content')
    @include('page.operasi.common.success')
    @include('page.operasi.common.delete_success')

    <div class="floating_alert">
        @include('common.errors')
    </div>

    <div ng-controller="inputPerkaraCtrl">
        @include('page.operasi.common.title', ['title' => 'Perkara'])

        @include('page.operasi.common.cari_nomor_perkara', ['route' => route('perkara')])

        @if ($perkara != '')
            {{-- Data tersimpan --}}
            <div class="">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-title">
                            Data Tersimpan
                        </div>
                    </div>
                    <div class="panel-body">

                        <form class="form-horizontal" novalidate="">
                            <h4 class="text-center text-info">Perkara</h4>
                            <div class="ln_solid"></div>
                            @include('page.operasi.common.common_data')
                        </form>
                    </div>
                </div>
            </div>

            @include('page.operasi.perkara.ubah_perkara')
        @else
            @include('page.operasi.perkara.tambah_perkara')
        @endif
    </div>
@endsection
