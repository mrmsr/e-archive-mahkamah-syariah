{{-- Input data baru --}}
<div class="">
    <div class="panel panel-info">
        <div class="panel-heading">
            <div class="panel-title">
                Data Perkara Baru
            </div>
        </div>
        <div class="panel-body">

            <form class="form-horizontal" name="form_perkara" novalidate="" action="{{route('perkara_tambah')}}" method="post">
                {{ csrf_field() }}
                <div class="row item form-group @if($errors->has('nomor_perkara')){{'has-error'}}@endif">
                    <div class="col-md-3">
                        <label class="control-label pull-right" for="">Nomor Perkara</label>
                    </div>
                    <div class="col-md-2">
                        <input type="text" class="form-control"
                        minlength="4"
                        ng-pattern="/^\d+$/"
                        name="k1"
                        required
                        ng-model="kode1">
                    </div>
                    <div class="col-md-2">
                        <select name="" id="" class="form-control" required name="k2" ng-model="kode2">
                            <option value="P">P</option>
                            <option value="G">G</option>
                        </select>
                    </div>
                    <div class="col-md-2">
                        <input type="number" min="1974" max="2100" class="form-control" name="k3" required ng-model="kode3">
                    </div>
                    <div class="col-md-offset-3 col-md-9 text-danger" ng-if="form_perkara.$dirty">
                        <span ng-if="form_perkara.k1.$error.required">Masukkan kode perkara. </span>
                        <span ng-if="form_perkara.k1.$error.minlength">Kode Perkara minimal berisi 4 angka. </span>
                        <span ng-if="form_perkara.k1.$error.pattern">Kode perkara yang dimasukkan harus angka. </span>

                        <span ng-if="form_perkara.k3.$error.required">Masukkan tahun pada kode perkara.  </span>
                        <span ng-if="form_perkara.k3.$error.min || form_perkara.k3.$error.max">Tahun berada dalam rentang 1974 - 2100. </span>
                    </div>
                </div>
                <div class="row item form-group">
                    <div class="col-md-3">
                        <label for="" class="control-label pull-right"></label>
                    </div>
                    <div class="col-md-6">
                        <input class="form-control" type="hidden" name="nomor_perkara" ng-value="kode1 + '/Pdt.' + kode2 + '/' + kode3 + '/MS-Bna'">
                        @{{ kode1 + '/Pdt.' + kode2 + '/' + kode3 + '/MS-Bna' }}
                        @if($errors->has('nomor_perkara'))
                            <p class="text-danger">
                                {{ $errors->first('nomor_perkara')}}
                            </p>
                        @endif
                    </div>

                </div>
                <div class="row item form-group @if($errors->has('jenis_perkara')){{'has-error'}}@endif" ng-if="kode2 === 'G'">
                    <div class="col-md-3">
                        <label class="control-label pull-right" for="jenis_perkara">Jenis Perkara
                        </label>
                    </div>
                    <div class="col-md-6">
                        <div ng-hide="true" class="ng-hide"></div>
                        <div class="radio-inline">
                            <label>
                                <input ng-model="$parent.jenis_perkara" name="jenis_perkara" type="radio" value="cerai"> Cerai
                            </label>
                        </div>
                        <div class="radio-inline">
                            <label>
                                <input ng-model="$parent.jenis_perkara" name="jenis_perkara" type="radio" value="lain-lain"> Lain-lain
                            </label>
                        </div>
                    </div>
                </div>
                <div class="row item form-group @if($errors->has('nama_pemohon')){{'has-error'}}@endif">
                    <div class="col-md-3">
                        <label class="control-label pull-right" for="nama_pemohon">Nama Pemohon/Penggugat
                    </label>
                    </div>
                    <div class="col-md-6">
                        <input type="text"
                               ng-model="nama_pemohon"
                               name="nama_pemohon"
                               class="form-control col-md-7 col-xs-12">
                        @if($errors->has('nama_pemohon'))
                            <p class="text-danger">
                                {{ $errors->first('nama_pemohon')}}
                            </p>
                        @endif
                    </div>
                </div>
                <div class="row item form-group @if($errors->has('nama_termohon')){{'has-error'}}@endif">
                    <div class="col-md-3">
                        <label class="control-label pull-right" for="nama_termohon">Nama Termohon/Tergugat
                    </label>
                    </div>
                    <div class="col-md-6">
                        <input type="text"
                               ng-model="nama_termohon"
                               name="nama_termohon"
                               required="required"
                               class="form-control">
                       @if($errors->has('nama_termohon'))
                           <p class="text-danger">
                               {{ $errors->first('nama_termohon')}}
                           </p>
                       @endif
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-6 col-md-offset-3">
                        <button type="submit" class="btn btn-success" ng-disabled="form_perkara.k1.$invalid || form_perkara.k2.$invalid || form_perkara.k3.$invalid">Tambah</button>
                        <span class="text-info" ng-if="form_perkara.k1.$invalid || form_perkara.k2.$invalid || form_perkara.k3.$invalid">
                            <span ng-if="form_perkara.$dirty">
                                Tidak dapat disubmit. Mohon nomor perkara diperiksa kembali.
                            </span>
                        </span>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
