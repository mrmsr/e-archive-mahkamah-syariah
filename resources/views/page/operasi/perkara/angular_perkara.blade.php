<script type="text/javascript">
    angular.module('eArchiveApp').controller('inputPerkaraCtrl', function($scope) {
        $scope.kode1 = '{{ old('k1')}}';
        $scope.kode2 = "{{ old('k2') }}";
        $scope.kode3 = parseInt("{{ old('k3') }}");

        @if ($perkara != '' and $no = $perkara->nomor_perkara)
            @if (preg_match('/^(.*?)\//', $no, $matches))
                if (!$scope.kode1) {
                    $scope.kode1 = '{{$matches[1]}}';
                }
            @endif

            @if (strpos($no, 'Pdt.G'))
                if (!$scope.kode2) {
                    $scope.kode2 = 'G';
                }
            @endif

            @if (preg_match('/pdt.[pg]\/(\d+)\//i', $no, $matches))
                if (!$scope.kode3) {
                    $scope.kode3 = parseInt('{{ $matches[1]}}');
                }
            @endif

            $scope.jenis_perkara = '{{ $perkara->jenis_perkara or '' }}';
        @endif

        if (!$scope.kode2) {
            $scope.kode2 = 'P';
        }

        if (!$scope.kode3) {
            $scope.kode3 = 2016;
        }

        if ($scope.jenis_perkara != 'cerai') {
            $scope.jenis_perkara = 'lain-lain';
        }

        $scope.nama_pemohon = '{{old('nama_pemohon')}}';
        $scope.nama_termohon = '{{old('nama_termohon')}}';

        @if ($perkara != '')
            if (!$scope.nama_pemohon) {
                $scope.nama_pemohon = '{{ $perkara->nama_pemohon or ''}}';
            }

            if(!$scope.nama_termohon) {
                $scope.nama_termohon = '{{ $perkara->nama_termohon or ''}}';
            }
        @endif

    });
</script>
