@extends('layout.app')

@section('content')

    @include('page.operasi.common.success')
    @include('page.operasi.common.delete_success')

    <div class="floating_alert">
        @include('common.errors')
    </div>

    <div ng-controller="mainCtrl">
        @include('page.operasi.common.title', ['title' => 'Akta Cerai'])
        @include('page.operasi.common.cari_nomor_perkara', ['route' => route('akta_cerai')])

        @if ($perkara !== "")
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title">
                        Data Tersimpan
                    </div>
                </div>
                <div class="panel-body">

                    <form class="form-horizontal" novalidate="">
                        <h4 class="text-center text-info">Perkara</h4>
                        <div class="ln_solid"></div>

                        @include('page.operasi.common.common_data')

                        <h4 class="text-center text-info">Akta Cerai</h4>
                        <div class="ln_solid"></div>

                        <div class="row">
                            <b class="col-md-3 text-right">Tanggal Pengambilan</b>
                            <div class="col-md-6">
                                {{ $perkara->ambilAktaCerai->first()->tanggal_pengambilan or "" }}
                            </div>
                        </div>
                        <div class="row">
                            <b class="col-md-3 text-right" >Nama Pengambil</b>
                            <div class="col-md-6">
                                {{ $perkara->ambilAktaCerai->first()->nama_pengambil or ""}}
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            <div class="panel panel-success">
                <div class="panel-heading">
                    <div class="panel-title">
                        Ubah Akta Cerai
                        <div class="pull-right">
                            <a href="{{route('akta_cerai_delete', ['id' => "$perkara->id"])}}">
                                <i class="fa fa-trash"></i>
                            </a>
                        </div>
                        <ul class="nav navbar-right panel_toolbox">
                            <li class="pull-right">
                            </li>
                        </ul>
                    </div>

                </div>
                <div class="panel-body">

                    <form class="form-horizontal" novalidate="" method="post"
                          action="{{route('akta_cerai_edit')}}">

                        @include('common.errors')

                        {{ csrf_field() }}
                        <input type="hidden" name="id" ng-model="id" value="{{$perkara->id}}">
                        <div class="row item form-group @if($errors->has('tanggal_pengambilan')){{'has-error'}}@endif">
                            <label for="" class="control-label col-md-3">Tanggal Pengambilan</label>
                            <div class="col-md-6">
                                <input data-inputmask="'mask': 'd/m/y'"
                                       name="tanggal_pengambilan"
                                       placeholder="dd/mm/yyyy" type="text" class="form-control"
                                       value="@if(old('tanggal_pengambilan') != ""){{old('tanggal_pengambilan')}}@else{{$perkara->tanggal_pengambilan or ""}}@endif">
                                @if($errors->has('tanggal_pengambilan'))
                                    <p class="text-danger">
                                        {{ $errors->first('tanggal_pengambilan') }}
                                    </p>
                                @endif
                            </div>
                        </div>
                        <div class="row item form-group @if($errors->has('nama_pengambil')){{'has-error'}}@endif">
                            <label for="" class="control-label col-md-3">Nama Pengambil</label>
                            <div class="col-md-6">
                                <input type="text"
                                       name="nama_pengambil"
                                       class="form-control"
                                       value="@if(old('nama_pengambil') != ""){{old('nama_pengambil')}}@else{{$perkara->nama_pengambil or ""}}@endif">
                                @if($errors->has('nama_pengambil'))
                                    <p class="text-danger">
                                        {{ $errors->first('nama_pengambil') }}
                                    </p>
                                @endif
                            </div>
                        </div>
                        <div class="ln_solid"></div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-3">
                                <button type="submit" class="btn btn-success">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        @endif
    </div>
@endsection

@section('additiona_script')

@endsection
