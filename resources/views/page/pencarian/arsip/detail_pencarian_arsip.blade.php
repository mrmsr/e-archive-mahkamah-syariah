
<div class="modal fade" id="detail_arsip" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title text-primary">Detail Arsip</h4>
            </div>

            <div class="modal-body">
                <div class="form-horizontal form-label-left" novalidate="">
                    <h4 class="form-section text-primary text-center">
                        <a href="{{route('perkara')}}?perkara_id=@{{ detail.id }}" target="_blank">
                            Perkara
                        </a>
                    </h4>
                    <div class="ln_solid"></div>
                    <div class="row">
                        <p class="col-md-4 text-right">Nomor Perkara</p>
                        <p class="col-md-8 text-info">@{{ detail.nomor_perkara }}</p>
                    </div>
                    <div class="row">
                        <p class="col-md-4 text-right">Nama Pemohon/Penggugat</p>
                        <p class="col-md-8 text-info">@{{ detail.nama_pemohon }}</p>
                    </div>
                    <div class="row">
                        <p class="col-md-4 text-right">Nama Termohon/Tergugat</p>
                        <p class="col-md-8 text-info">@{{ detail.nama_termohon }}</p>
                    </div>
                    <div class="row">
                        <p class="col-md-4 text-right">Jenis Perkara</p>
                        <p class="col-md-8 text-info">@{{ detail.jenis_perkara }}</p>
                    </div>

                    <h4 class="form-section text-primary text-center">
                        <a href="{{route('amar_putusan')}}?perkara_id=@{{ detail.id }}" target="_blank">
                            Amar Putusan
                        </a>
                    </h4>
                    <div class="ln_solid"></div>

                    <div class="row">
                        <p class="col-md-4 text-right">Amar Putusan</p>
                        <p class="col-md-8 text-info"><textarea class="form-control" rows="5" disabled>@{{ detail.amar_putusan }}</textarea></p>
                    </div>
                    <div class="row">
                        <p class="col-md-4 text-right">Tanggal Putus</p>
                        <p class="col-md-8 text-info">@{{ detail.tanggal_putus }}</p>
                    </div>
                    <div class="row">
                        <p class="col-md-4 text-right">Tanggal BHT</p>
                        <p class="col-md-8 text-info">@{{ detail.tanggal_bht }}</p>
                    </div>

                    <h4 class="form-section text-primary text-center">
                        <a href="{{route('amar_putusan')}}?perkara_id=@{{ detail.id }}" target="_blank">
                                Letak Berkas
                        </a>
                    </h4>
                    <div class="ln_solid"></div>

                    <div class="row">
                        <p class="col-md-4 text-right">Status Perkara</p>
                        <p class="col-md-8 text-info">@{{ detail.status_perkara }}</p>
                    </div>
                    <div class="row">
                        <p class="col-md-4 text-right">Letak Berkas</p>
                        <p class="col-md-8 text-info">@{{ detail.letak_berkas }}</p>
                    </div>

                    <h4 class="form-section text-primary text-center">Info Ambil Berkas</h4>
                    <div class="ln_solid"></div>

                    <div class="row">
                        <p class="col-md-4 text-right">
                            <a href="{{route('salinan_putusan')}}?perkara_id=@{{ detail.id }}" target="_blank">
                                Salinan Putusan
                            </a>
                        </p>
                        <p class="col-md-8 text-info">
                            <span ng-if="detail.salinan_putusan.nama_pengambil || detail.salinan_putusan.tanggal_pengambilan">
                                Sudah diambil oleh
                                "@{{ detail.salinan_putusan.nama_pengambil }}" pada tanggal
                                "@{{ detail.salinan_putusan.tanggal_pengambilan }}"
                            </span>
                        </p>
                    </div>

                    <div class="row">
                        <p class="col-md-4 text-right">
                            <a href="{{route('akta_cerai')}}?perkara_id=@{{ detail.id }}" target="_blank">
                                Akta Cerai
                            </a>
                        </p>
                        <p class="col-md-8 text-info">
                            <span ng-if="detail.akta_cerai.nama_pengambil || detail.akta_cerai.tanggal_pengambilan">
                                Sudah diambil oleh
                                "@{{ detail.akta_cerai.nama_pengambil }}"
                                pada tanggal
                                "@{{ detail.akta_cerai.tanggal_pengambilan }}"
                            </span>
                        </p>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>
