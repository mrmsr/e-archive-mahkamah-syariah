@extends('layout.app')

@section('additional_script')
    @include('page.pencarian.arsip.angular_pencarian_arsip')
@endsection

@section('content')
    <div ng-controller="pencarianArsipCtrl">
        @include('page.pencarian.arsip.hasil_pencarian_arsip')
        @include('page.pencarian.arsip.detail_pencarian_arsip')
    </div>
@endsection
