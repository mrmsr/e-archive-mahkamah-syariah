<div class="panel panel-primary">
    <div class="panel-heading">
        <div class="panel-title">
            <h4>Data Arsip</h4>
        </div>
    </div>
    <div class="panel-body">
        {{-- Form cari --}}
        <div class="row">
            <div class="col-md-5 pull-right form-group">
                <form action="{{route('pencarian_arsip')}}" autocomplete="off">
                    <div class="input-group">
                        <input class="form-control" name="key"
                               value="{{ app('request')->input('key') }}"
                               type="text">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="submit">Cari</button>
                        </span>
                    </div>
                </form>
            </div>
        </div>
        <div class="clearfix">

        </div>

        {{-- Tabel --}}
        <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Nomor Perkara</th>
                        <th>Pemohon/Penggugat</th>
                        <th>Termohon/Tergugat</th>
                        <th>Detail</th>
                    </tr>
                </thead>
                <tbody>
                    <tr ng-repeat="p in perkara">
                        <th scope="row">@{{ $index+1 }}</th>
                        <td>@{{ p.nomor_perkara }}</td>
                        <td>@{{ p.nama_pemohon }}</td>
                        <td>@{{ p.nama_termohon }}</td>
                        <td>
                            <button class="btn btn-default btn-md" type="button" name="button"
                                    data-toggle="modal" data-target="#detail_arsip" ng-click="copyDetail(p)">
                                Detail
                            </button>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>

        <div class="row">
            <div class="col-md-6">
                <br>
                @if ($perkara->total() > 0)
                    Menampilkan {{$perkara->firstItem()}} - {{$perkara->lastItem()}} dari total
                @else
                    Total
                @endif
                {{ $perkara->total() }} data.
            </div>
            <div class="col-md-6 text-right">
                {{ $perkara->links() }}
            </div>
        </div>
    </div>
</div>
