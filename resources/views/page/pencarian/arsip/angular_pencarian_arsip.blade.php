<script type="text/javascript">
    angular.module('eArchiveApp').controller('pencarianArsipCtrl',function($scope) {
        $scope.perkara = perkara
        $scope.detail = {}

        $scope.copyDetail = function(data) {
            $scope.detail = angular.copy(data)
        }
    })

    perkara = [
        @foreach($perkara as $p)
            {
                'id': '{{$p->id }}',
                'nomor_perkara': '{{$p->nomor_perkara or ""}}',
                'nama_pemohon' : '{{$p->nama_pemohon or ""}}',
                'nama_termohon': '{{$p->nama_termohon or ""}}',
                'jenis_perkara': '{{$p->jenis_perkara or ""}}',

                @if ($ap = $p->amarPutusan()->first())
                    'amar_putusan' : '{{$ap->amar_putusan or ''}}',
                    'tanggal_putus' : '{{$ap->tanggal_putus or ''}}',
                    'tanggal_bht' : '{{$ap->tanggal_bht or ''}}',
                @endif

                'status_perkara': '{{$p->status_perkara or ''}}',
                'letak_berkas' : '{{$p->letakBerkas()->first()->letak_berkas or ''}}',

                @if ($sp = $p->ambilSalinanPutusan()->first())
                    'salinan_putusan' : {
                        'nama_pengambil' : '{{$sp->nama_pengambil or ''}}',
                        'tanggal_pengambilan' : '{{$sp->tanggal_pengambilan or ''}}',
                    },
                @endif

                @if ($ac = $p->ambilAktaCerai()->first())
                    'akta_cerai' : {
                        'nama_pengambil' : '{{$ac->nama_pengambil or ''}}',
                        'tanggal_pengambilan' : '{{$ac->tanggal_pengambilan or ''}}',
                    }
                @endif
            },
        @endforeach
    ]
</script>
