<?php

use Illuminate\Database\Seeder;
use App\User;

class FirstUser extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        User::create([
            'name' => 'Root Admin',
            'username' => 'root',
            'password' => bcrypt('something_secret')
        ]);
    }
}
