<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Perkara extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('perkara', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nomor_perkara')->unique();

            $table->string('jenis_perkara')->nullable();
            $table->string('nama_pemohon')->nullable();
            $table->string('nama_termohon')->nullable();
            $table->string('status_perkara')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop("perkara");
    }
}
