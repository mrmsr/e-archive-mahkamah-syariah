<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class LetakBerkas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('letak_berkas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('letak_berkas')->nullable();

            $table->integer('perkara_id')->unisgned();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop("letak_berkas");
    }
}
