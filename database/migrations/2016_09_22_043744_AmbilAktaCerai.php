<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AmbilAktaCerai extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('ambil_akta_cerai', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama_pengambil');
            $table->date('tanggal_pengambilan')->nullable();

            $table->integer('perkara_id')->unisgned();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop("ambil_akta_cerai");
    }
}

