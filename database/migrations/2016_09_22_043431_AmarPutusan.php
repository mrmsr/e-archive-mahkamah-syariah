<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AmarPutusan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('amar_putusan', function (Blueprint $table) {
            $table->increments('id');
            $table->longText('amar_putusan')->nullable();
            $table->date('tanggal_putus')->nullable();
            $table->date('tanggal_bht')->nullable();

            $table->integer('perkara_id')->unisgned();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop("amar_putusan");
    }
}
