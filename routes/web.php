<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/


Route::group(['middleware' => 'web'], function() {
    Route::get('cari_nomor_perkara', [
        'uses' => 'PencarianArsipController@cariNomorPerkara',
        'as' => 'cari_nomor_perkara',
    ]);

    Route::get('/', function () {
        // return view('welcome');
        return redirect()->route('home');
    });

    Route::get('/home', function() {
        return view('home');
    })->name('home');

    // Ubah password
    Route::group(['prefix' => 'ubah_password', 'middleware' => 'auth'], function() {
        Route::get('/', [
            'uses' => 'Auth\UbahPasswordController@showUbahPasswordForm',
            'as' => 'ubah_password',
        ]);
        Route::post('/', [
            'uses' => 'Auth\UbahPasswordController@ubahPassword',
            'as' => 'ubah_password',
        ]);
    });

    Route::group(['prefix' => 'user_profile'], function() {
        Route::get('/', [
            'uses' => 'Auth\ProfileController@getFormProfile',
            'as' => 'user_profile'
        ]);

        Route::post('/', [
            'uses' => 'Auth\ProfileController@postFormProfile',
            'as' => 'post_user_profile',
        ]);
    });

    // Pencarian arsip
    Route::get('/pencarian_arsip', [
        'uses' => 'PencarianArsipController@index',
        'as' => 'pencarian_arsip'
    ]);

    // Operasi input, edit, dan hapus
    Route::group(['prefix' => 'operasi', 'middleware' => 'auth'], function() {

        // Edit perkara
        Route::group(['prefix' => 'perkara'], function() {
            Route::get('/', [
                'uses' => 'PerkaraController@perkara',
                'as' => 'perkara'
            ]);
            Route::post('/add', [
                'uses' => 'PerkaraController@addPerkara',
                'as' => 'perkara_tambah'
            ]);
            Route::post('/edit', [
                'uses' => 'PerkaraController@editPerkara',
                'as' => 'perkara_edit'
            ]);

            Route::get('/delete', [
                'uses' => 'PerkaraController@deletePerkara',
                'as' => 'perkara_delete'
            ]);
        });

        // Edit amar putusan
        Route::group(['prefix' => 'amar_putusan'], function() {
            Route::get('/', [
                'uses' => 'AmarPutusanController@amarPutusan',
                'as' => 'amar_putusan'
            ]);
            Route::post('/edit', [
                'uses' => 'AmarPutusanController@editAmarPutusan',
                'as' => 'amar_putusan_edit'
            ]);
            Route::get('/delete', [
                'uses' => 'AmarPutusanController@deleteAmarPutusan',
                'as' => 'amar_putusan_delete'
            ]);
        });

        // Edit akta cerai
        Route::group(['prefix' => 'akta_cerai'], function() {
            Route::get('/', [
                'uses' => 'AktaCeraiController@aktaCerai',
                'as' => 'akta_cerai'
            ]);
            Route::post('/edit', [
                'uses' => 'AktaCeraiController@editAktaCerai',
                'as' => 'akta_cerai_edit'
            ]);
            Route::get('/delete', [
                'uses' => 'AktaCeraiController@deleteAktaCerai',
                'as' => 'akta_cerai_delete'
            ]);
        });

        // Edit salinan putusan
        Route::group(['prefix' => 'salinan_putusan'], function() {
            Route::get('/', [
                'uses' => 'SalinanPutusanController@salinanPutusan',
                'as' => 'salinan_putusan'
            ]);
            Route::post('/edit', [
                'uses' => 'SalinanPutusanController@editSalinanPutusan',
                'as' => 'salinan_putusan_edit'
            ]);
            Route::get('/delete', [
                'uses' => 'SalinanPutusanController@deleteSalinanPutusan',
                'as' => 'salinan_putusan_delete'
            ]);
        });


    });

});

Auth::routes();
