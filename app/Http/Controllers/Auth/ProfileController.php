<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    //
    public function getFormProfile() {
        return view('auth.user_profile');
    }

    public function postFormProfile(Request $req) {
        $this->validate($req, [
            'nama' => 'required|min:3|max:255',
        ]);

        $user = Auth::user();
        $user->name = $req->nama;
        $user->save();

        return redirect()->back()->with(['success' => 'Data berhasil disubmit.']);
    }
}
