<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    // use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    public function showLoginForm() {
        return view('auth.login');
    }

    public function login(Request $req) {
        $credentials = [
            'username' => $req->username,
            'password' => $req->password,
        ];

        if (Auth::attempt($credentials)) {
            return redirect()->route('pencarian_arsip');
        }

        return redirect()->back()->withErrors([
            'message' => 'username atau password tidak valid.'
        ]);
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/');
    }
}
