<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\User;

class UbahPasswordController extends Controller
{
    //
    public function __construct() {
        $this->middleware('auth');
    }

    public function showUbahPasswordForm() {
        return view('auth.ubah_password');
    }

    public function ubahPassword(Request $req) {
        $this->validate($req, [
            'id' => 'required',
            'old_password' => 'required',
            'new_password' => 'required|min:5|max:255',
            'confirm' => 'required|same:new_password',
        ]);

        $old = $req->old_password;
        $new = $req->new_password;

        $user = User::findOrFail($req->id);

        if (!Hash::check($old, $user->password)) {
            return redirect()->back()->withErrors(['old_password' => 'Password lama tidak cocok.']);
        }

        $user->password = bcrypt($new);
        $user->save();

        return redirect()->back()->with(['success' => 'Password berhasil diubah.']);
    }
}
