<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Perkara;

class PerkaraController extends Controller
{
    //
    public function perkara(Request $request) {
        $perkara = "";
        if ($request->has('nomor_perkara')) {
            $nomor = $request->input('nomor_perkara');

            $perkara = Perkara::where('nomor_perkara', 'LIKE', "%$nomor%")->first();
            if (!$perkara) {
                return view('page.operasi.perkara.perkara',  [
                    'perkara' => ""
                ])->withErrors(['Error' => 'Pencarian tidak ditemukan.']);
            }
        }
        else if ($request->has('perkara_id')) {
            $id = $request->input('perkara_id');
            $perkara = Perkara::findOrFail($id);
        }

        return view('page.operasi.perkara.perkara', [
            'perkara' => $perkara
        ]);
    }

    public function addPerkara(Request $request) {
        $this->validate($request, [
            'nomor_perkara' => 'required|unique:perkara,nomor_perkara',
            'jenis_perkara' => '',
            'nama_pemohon' => 'required|min:3|max:255',
            'nama_termohon' => 'required|min:3|max:255',
        ]);

        $perkara = Perkara::create([
            'nomor_perkara' => $request->nomor_perkara,
            'jenis_perkara' => $request->jenis_perkara,
            'nama_pemohon' => $request->nama_pemohon,
            'nama_termohon' => $request->nama_termohon,
        ]);

        return redirect()->route('perkara', ['perkara_id' => $perkara->id])->with(['success' => '<strong>Success</strong> Data perkara berhasil disimpan.']);
    }

    public function editPerkara(Request $request) {
        $this->validate($request, [
            'id' => 'required',
            'nomor_perkara' => 'unique:perkara,nomor_perkara',
            'jenis_perkara' => '',
            'nama_pemohon' => 'required|min:3|max:255',
            'nama_termohon' => 'required|min:3|max:255',
        ]);

        $perkara = Perkara::findOrFail($request->id);

        if ($request->nomor_perkara) {
            $perkara->update([
                'nomor_perkara' => $request->nomor_perkara,
            ]);
        }

        $perkara->update([
            'jenis_perkara' => $request->jenis_perkara,
            'nama_pemohon' => $request->nama_pemohon,
            'nama_termohon' => $request->nama_termohon,
        ]);

        $message = ['success' => '<strong>Success</strong> Data perkara berhasil diubah.'];
        if ($request->nomor_perkara) {
            return redirect()->route('perkara', ['nomor_perkara' => $request->nomor_perkara])->with($message);
        }
        return back()->with($message);
    }

    public function deletePerkara(Request $request) {
        $this->validate($request, [
            'id' => 'required',
        ]);

        $perkara = Perkara::findOrFail($request->id);

        $perkara->letakBerkas()->delete();
        $perkara->amarPutusan()->delete();
        $perkara->ambilSalinanPutusan()->delete();
        $perkara->ambilAktaCerai()->delete();

        $perkara->delete();

        return redirect()->route('perkara')->with(['delete_success' => '<strong>Success</strong> Data perkara berhasil dihapus.']);
    }





}
