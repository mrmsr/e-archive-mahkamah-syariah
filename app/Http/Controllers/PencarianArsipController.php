<?php

namespace App\Http\Controllers;

use App\Perkara;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Input;

class PencarianArsipController extends Controller
{
    //
    public function index(Request $request) {
        if ($request->has('key')) {
            $key = $request->input('key');
            $perkara = Perkara::where('nomor_perkara', 'LIKE', "%$key%")
                ->orWhere('nama_pemohon', 'LIKE', "%$key%")
                ->orWhere('nama_termohon', 'LIKE', "%$key%")
                ->paginate(5);
            $perkara->appends(Input::except('page'));
        } else {
            $perkara = Perkara::paginate(5);
        }

        return view('page.pencarian.arsip.pencarian_arsip', [
            'perkara' => $perkara,
        ]);
    }

    // Api
    public function cariNomorPerkara(Request $request) {
        $key = $request->key;
        $perkara = Perkara::where('nomor_perkara', 'LIKE', "%$key%")->select('nomor_perkara')->limit(15)->get();
        return $perkara;
    }
}
