<?php

namespace App\Http\Controllers;

use App\Perkara;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;

class SalinanPutusanController extends Controller
{
    public function salinanPutusan(Request $request) {
        $perkara = "";
        if ($request->has('nomor_perkara')) {
            $nomor = $request->input('nomor_perkara');

            $perkara = Perkara::where('nomor_perkara', 'LIKE', "%$nomor%")->first();
            if (!$perkara) {
                return view('page.operasi.salinan_putusan.salinan_putusan',  [
                    'perkara' => ""
                ])->withErrors(['Error' => 'Pencarian tidak ditemukan.']);
            }
        }
        else if ($request->has('perkara_id')) {
            $id = $request->input('perkara_id');
            $perkara = Perkara::findOrFail($id);

        }

        if ($perkara !== "" and $sp = $perkara->ambilSalinanPutusan()->first()) {

            $perkara['nama_pengambil'] = $sp->nama_pengambil;
            $tgl = null;
            if ($sp->tanggal_pengambilan) {
                $tgl = \DateTime::createFromFormat('Y-m-d', $sp->tanggal_pengambilan)->format('d/m/Y');
            }
            $perkara['tanggal_pengambilan'] = $tgl;
        }



        return view('page.operasi.salinan_putusan.salinan_putusan', [
            'perkara' => $perkara
        ]);
    }

    public function editSalinanPutusan(Request $request) {
        $this->validate($request, [
            'id' => 'required',
            'tanggal_pengambilan' => 'required|date_format:d/m/Y',
            'nama_pengambil' => 'required|min:3|max:255',
        ]);

        $perkara = Perkara::find($request->input('id'));
        if ($perkara) {
            $tgl = null;
            if ($request->has('tanggal_pengambilan')) {
                $tgl = Carbon::createFromFormat('d/m/Y', $request->tanggal_pengambilan)->toDateString();
            }

            if ($sp = $perkara->ambilSalinanPutusan()->first()) {
                $sp->update([
                    'tanggal_pengambilan' => $tgl,
                    'nama_pengambil' => $request->input('nama_pengambil'),
                ]);

            } else {
                $perkara->ambilSalinanPutusan()->create([
                    'tanggal_pengambilan' => $tgl,
                    'nama_pengambil' => $request->input('nama_pengambil'),
                ]);
            }
            return redirect()->back()->with(['success' => '<strong>Success</strong> Data salinan putusan berhasil disimpan']);
        }

        return redirect()->back()->withErrors(['error' => 'Gagal melakukan input.']);
    }

    public function deleteSalinanPutusan(Request $request) {
        $this->validate($request, [
            'id' => 'required'
        ]);

        $perkara = Perkara::findOrFail($request->id);

        $perkara->ambilSalinanPutusan()->delete();

        return back()->with(['delete_success' => '<strong>Success</strong> Data salinan putusan berhasil dihapus']);
    }
}
