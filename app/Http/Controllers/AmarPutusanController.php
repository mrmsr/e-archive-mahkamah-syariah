<?php

namespace App\Http\Controllers;

use App\Perkara;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;

class AmarPutusanController extends Controller
{
    //
    public function amarPutusan(Request $request) {
        $perkara = "";
        if ($request->has('nomor_perkara')) {
            $nomor = $request->input('nomor_perkara');

            $perkara = Perkara::where('nomor_perkara', 'LIKE', "%$nomor%")->first();
            if (!$perkara) {
                return view('page.operasi.amar_putusan.amar_putusan',  [
                    'perkara' => ""
                ])->withErrors(['Error' => 'Pencarian tidak ditemukan.']);
            }
        }
        else if ($request->has('perkara_id')) {
            $id = $request->input('perkara_id');
            $perkara = Perkara::findOrFail($id);
        }

        if ($perkara !== "") {

            if ($sp = $perkara->amarPutusan()->first()) {

                $perkara['amar_putusan'] = $sp->amar_putusan;
                $tgl = null;
                if ($sp->tanggal_putus) {
                    $tgl = \DateTime::createFromFormat('Y-m-d', $sp->tanggal_putus)->format('d/m/Y');
                }
                $perkara['tanggal_putus'] = $tgl;

                $tgl = null;
                if ($sp->tanggal_bht) {
                    $tgl = \DateTime::createFromFormat('Y-m-d', $sp->tanggal_bht)->format('d/m/Y');
                }
                $perkara['tanggal_bht'] = $tgl;
            }

            if ($lb = $perkara->letakBerkas()->first()) {
                $perkara['letak_berkas'] = $lb->letak_berkas;
            }

        }


        return view('page.operasi.amar_putusan.amar_putusan', [
            'perkara' => $perkara
        ]);
    }

    public function editAmarPutusan(Request $request) {
        $this->validate($request, [
            'id' => 'required',
            'amar_putusan' => 'required',
            'tanggal_putus' => 'required|date_format:d/m/Y',
            'tanggal_bht' => 'required|date_format:d/m/Y',
            'status_perkara' => 'required',
            'letak_berkas' => 'required_if:status_perkara,statis',
        ]);

        $perkara = Perkara::find($request->input('id'));
        if ($perkara) {
            $tgl_putus = null;
            if ($request->has('tanggal_putus')) {
                $tgl_putus = Carbon::createFromFormat('d/m/Y', $request->tanggal_putus)->toDateString();
            }

            $tgl_bht = null;
            if ($request->has('tanggal_bht')) {
                $tgl_bht = Carbon::createFromFormat('d/m/Y', $request->tanggal_bht)->toDateString();
            }

            if ($ap = $perkara->amarPutusan()->first()) {
                $ap->update([
                    'amar_putusan' => $request->input('amar_putusan'),
                    'tanggal_putus' => $tgl_putus,
                    'tanggal_bht' => $tgl_bht,
                ]);
            } else {
                $perkara->amarPutusan()->create([
                    'amar_putusan' => $request->input('amar_putusan'),
                    'tanggal_putus' => $tgl_putus,
                    'tanggal_bht' => $tgl_bht,
                ]);
            }

            $perkara->update(['status_perkara' => $request->input('status_perkara')]);

            if ($lb = $perkara->letakBerkas()->first()) {
                $perkara->letakBerkas()->update([
                    'letak_berkas' => $request->input('letak_berkas'),
                ]);
            } else {
                $perkara->letakBerkas()->create([
                    'letak_berkas' => $request->input('letak_berkas'),
                ]);
            }



            return redirect()->back()->with(['success' => '<strong>Success</strong> Data amar putusan berhasil disimpan']);
        }

        return redirect()->back()->withErrors(['error' => 'Gagal melakukan input.']);
    }

    public function deleteAmarPutusan(Request $request) {
        $this->validate($request, [
            'id' => 'required'
        ]);

        $perkara = Perkara::findOrFail($request->id);

        $perkara->update(['status_perkara' => null]);
        $perkara->amarPutusan()->delete();
        $perkara->letakBerkas()->delete();

        return back()->with(['delete_success' => '<strong>Success</strong> Data amar putusan berhasil dihapus']);
    }
}
