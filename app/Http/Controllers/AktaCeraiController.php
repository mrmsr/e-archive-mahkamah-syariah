<?php

namespace App\Http\Controllers;

use App\Perkara;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;

class AktaCeraiController extends Controller
{

    public function aktaCerai(Request $request) {
        $perkara = "";
        if ($request->has('nomor_perkara')) {
            $nomor = $request->input('nomor_perkara');

            $perkara = Perkara::where('nomor_perkara', 'LIKE', "%$nomor%")->first();
            if (!$perkara) {
                return view('page.operasi.akta_cerai.akta_cerai',  [
                    'perkara' => ""
                ])->withErrors(['Error' => 'Pencarian tidak ditemukan.']);
            }
        }
        else if ($request->has('perkara_id')) {
            $id = $request->input('perkara_id');
            $perkara = Perkara::findOrFail($id);
        }

        if ($perkara !== "") {
            if (strcasecmp($perkara->jenis_perkara, "cerai") != 0) {

                return view('page.operasi.akta_cerai.akta_cerai',  [
                    'perkara' => ""
                ])->withErrors(['Error' => 'Perkara tidak membutuhkan akta cerai.']);
            }

            if ($sp = $perkara->ambilAktaCerai()->first()) {

                $perkara['nama_pengambil'] = $sp->nama_pengambil;
                $tgl = null;
                if ($sp->tanggal_pengambilan) {
                    $tgl = \DateTime::createFromFormat('Y-m-d', $sp->tanggal_pengambilan)->format('d/m/Y');
                }
                $perkara['tanggal_pengambilan'] = $tgl;
            }
        }


        return view('page.operasi.akta_cerai.akta_cerai', [
            'perkara' => $perkara
        ]);
    }

    public function editAktaCerai(Request $request) {
        $this->validate($request, [
            'id' => 'required',
            'tanggal_pengambilan' => 'required|date_format:d/m/Y',
            'nama_pengambil' => 'required|min:3|max:255',
        ]);

        $perkara = Perkara::find($request->input('id'));
        if ($perkara) {
            $tgl = null;
            if ($request->has('tanggal_pengambilan')) {
                $tgl = Carbon::createFromFormat('d/m/Y', $request->tanggal_pengambilan)->toDateString();
            }

            if ($sp = $perkara->ambilAktaCerai()->first()) {
                $sp->update([
                    'tanggal_pengambilan' => $tgl,
                    'nama_pengambil' => $request->input('nama_pengambil'),
                ]);

            } else {
                $perkara->ambilAktaCerai()->create([
                    'tanggal_pengambilan' => $tgl,
                    'nama_pengambil' => $request->input('nama_pengambil'),
                ]);
            }
            return redirect()->back()->with(['success' => '<strong>Success</strong> Data akta cerai berhasil disimpan']);
        }

        return redirect()->back()->withErrors(['error' => 'Gagal melakukan input.']);
    }

    public function deleteAktaCerai(Request $request) {
        $this->validate($request, [
            'id' => 'required'
        ]);

        $perkara = Perkara::findOrFail($request->id);

        $perkara->ambilAktaCerai()->delete();

        return back()->with(['delete_success' => '<strong>Success</strong> Data akta cerai berhasil dihapus']);
    }
}
