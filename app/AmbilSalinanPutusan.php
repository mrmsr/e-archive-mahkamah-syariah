<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AmbilSalinanPutusan extends Model
{
    //
    protected $table = "ambil_salinan_putusan";
    protected $guarded = [];

    public function perkara() {
        return $this->belongsTo(Perkara::class);
    }
}
