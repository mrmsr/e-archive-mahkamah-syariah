<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Perkara extends Model
{
    //
    protected $table = "perkara";
    protected $guarded = [];

    public function letakBerkas() {
        return $this->hasOne(LetakBerkas::class);
    }

    public function amarPutusan() {
        return $this->hasOne(AmarPutusan::class);
    }

    public function ambilSalinanPutusan() {
        return $this->hasMany(AmbilSalinanPutusan::class);
    }


    public function ambilAktaCerai() {
        return $this->hasMany(AmbilAktaCerai::class);
    }
}
