<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LetakBerkas extends Model
{
    //
    protected $table = "letak_berkas";
    protected $guarded = [];

    public function perkara() {
        return $this->belongsTo(Perkara::class);
    }
}
