<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AmarPutusan extends Model
{
    //
    protected $table = "amar_putusan";
    protected $guarded = [];

    public function perkara() {
        return $this->belongsTo(Perkara::class);
    }
}
