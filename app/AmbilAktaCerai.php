<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AmbilAktaCerai extends Model
{
    //
    protected $table = "ambil_akta_cerai";
    protected $guarded = [];

    public function perkara() {
        return $this->belongsTo(Perkara::class);
    }
}
